export interface StockRequestModal {
    groupBy: string,
    viewAs?: string,
    distributorId: number
}
