export interface UserModel {
    id?: number;
    imageUrl?: string;
    name?: string;
    firstName?: string;
    middleName?: string;
    lastName?: string;
    email?: string;
    gender?: string;
    dob?: string;
    phone?: string;
    password?: string;
    lastLogin?: string;
    city?: string;
    state?: string;
    country?: string;
    address?: string;
    userType?: string;
    deviceToken?: string;
    status?: string;
    isLoggedIn?: number;
}
