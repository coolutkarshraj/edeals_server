export class Tables {
    public static user: string = "user";
    public static brand: string = "brand";
    public static product: string = "product";
    public static category: string = "category";
    public static products: string = "products";
    public static orders: string = "orders";
    public static inbox: string = "inbox";
}